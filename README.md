# my2018TODOlist
---
_This is my personal TODO list of goals for 2018_
---

> RATIO: (done) 7/11 (remaining)

## TODO:
* Build new portfolio website (Vue, Vuex, SSR, PWA, Web Animations, Push Notifications)
* Update Hasie szkło db for 2019

## IN PROGRESS:
* Build new PWA app - Estimator (Vue, Vuex, Firebase, Realtime DB, more)
* Build Arduino soil, moist, relays module (websockets, Vue, node.js)

## DONE:
* ~~Build and release social pictionary mobile game (websockets, Vue etc.)~~
* ~~Build Arduino photoresistor APP [office toilet status] (websockets, Vue, node.js)~~
* ~~Build and release Hasie, Szklo, i Bele co - Rybnik mobile app (push notifications, Vue etc.)~~
* ~~Build and release 2 player ripple'like mobile game (P5.js, Vue)~~
* ~~Build and release Animal Slider - mobile game w/ pathfinding (P5.js, pathfinding, Vue)~~
* ~~Build SVG Animation w/ anime.js for codepen.io~~
* ~~Develop an web-extension (Vue/node.js)~~

___

## OTHER (FUTURE) IDEAS:
* Electron app (task list/notepad/code snippets with markdown, and push notifications)
* Simple mobile game (pinball)
* Build/publish sth on github.com
* New showreel
